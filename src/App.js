import React from "react";
import HelloWorld from "./HelloWorld";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HelloWorld />
        <div className="App-div">
          <img
            src="https://i.pinimg.com/originals/6b/81/a1/6b81a1049b32cfa0fe2038fbc0a6d097.png"
            className="App-logo"
          />
          <img
            src="https://vignette.wikia.nocookie.net/liberproeliis/images/9/91/Rick_Sanchez_Render.png/revision/latest/scale-to-width-down/340?cb=20171011005606&path-prefix=pt-br"
            className="rick"
          />
          <img
            src="https://www.kindpng.com/picc/m/125-1250612_rick-and-morty-morty-png-transparent-png.png"
            className="morty"
          />
        </div>

        <h2 className="App-text"> React </h2>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
